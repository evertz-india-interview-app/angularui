import { PostToServerService } from '../post-to-server.service';
import { Component, OnInit, NgZone, Input } from '@angular/core';
import { Router, ActivatedRoute,NavigationExtras } from '@angular/router';
import { HttpErrorResponse,HttpResponse } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { SystemConfig } from '../ws-calls/system-config';

@Component({
  selector: 'app-candidate-registration',
  templateUrl: './candidate-registration.component.html',
  styleUrls: ['./candidate-registration.component.css']
})
export class CandidateRegistrationComponent implements OnInit {

  //Setting HTML Elements
  registerForm: FormGroup;
  loading = false;
  submitted = false;
  error: string;
  gender = ['Male','Female', 'Others'];
  collegeName: any;
  degree: any;
  stream: any;
  programme: any;

  //All Assignments used throught the code
  public serverIp = this.systemConfig.serverIp;
  public serverResponse = "";
  public requestList = "";
  public registrationStatus = "";
  public languageSelected: string = "";
  public serverStatus_Error_Class = "";
  public serverStatus_Error_Box = "";
  public serverStatus_Error_Message = "";
  public questionType = "MCQ";

  
  //Description: Function to Get the programming language choosed
  //Usage: To get the radio button value
  //Arg's: Takes html event as arguement
  radioChangeHandler(event: any){
    this.languageSelected = event.target.value;
  }

  //To bind serverIp for sending in WS-Call
  public dataToBeSent = {
    serverIp: this.serverIp
  };

  //To bind the Data List to be requested on WS-Call and bind it to HTML
  public dataListToBeRequested = {
    requestList: this.requestList
  };

  //Constructors for initialization 
  constructor(private postToServerObject: PostToServerService, private router: Router, public zone: NgZone, public activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private systemConfig: SystemConfig ) {
  }

  //Description: This module will get called onInit
  //Usage: To update college,branch,degree,Modules list on UI
  ngOnInit() {
          this.validateServer(this.serverIp);
          //this.candiateGenerateQuestoins("123");
          //updating college List
          this.requestList = "College";
          this.getDropDownList(this.requestList);
          //updating branch List
          this.requestList = "Branch";
          this.getDropDownList(this.requestList);
          //updating degree List
          this.requestList = "Degree";
          this.getDropDownList(this.requestList);
          //updating Modules List
          this.requestList = "Modules";
          this.getDropDownList(this.requestList);

          //vaiables used for validating data on HTML
          this.registerForm = this.formBuilder.group({

            registrationNumber: ['', [Validators.required,Validators.pattern("[A-Za-z0-9]{10}")]],
            firstName: ['',[Validators.required, Validators.pattern("[A-Za-z ]{2,}")]],
            dateOfBirth: ['', [Validators.required,Validators.pattern("(0[1-9]|1[0-9]|2[0-9]|3[01])-(0[1-9]|1[012])-[0-9]{4}")]],
            gender: ['', [Validators.required,Validators.nullValidator]],
            collegeName: ['', Validators.required],
            degree: ['', Validators.required],
            stream: ['',Validators.required],
            yearOfPassing: ['', [Validators.required,Validators.pattern("2020")]],
            overallCGPA: ['', [Validators.required, Validators.pattern("[0-9]+(\.[0-9]{0,2})?")]],
            personalEmailId: ['', [Validators.required,Validators.email]],
            personalMobileNumber: ['', [Validators.required,Validators.pattern("[0-9]{10}")]],
            programme: ['', Validators.required]

        });
        
      }

  //Description: Getter to validate the form elements
  //Usage: Set's the validation criteria to the HTML elements
  get formControls() { return this.registerForm.controls; }

  //Description: Function to validate the server connection
  //Usage: To validate the Server Before doing any post request
  //Arg's: Takes server IP to be validated as arguement
  public validateServer(serverIp) {

    this.dataToBeSent.serverIp = serverIp;

    this.postToServerObject.checkServer(this.dataToBeSent).subscribe((res: HttpResponse<any>)=> {
        console.log("Validating Server: " + this.serverIp)
        var serverStatus = "";
        this.serverResponse = JSON.parse(JSON.stringify(res.body));
        serverStatus = this.serverResponse["EvertzInterviewApp"]["Success"];
          if(!serverStatus)
          {
            window.scrollTo(0, 0);
            this.serverStatus_Error_Class = "isa_error";
            this.serverStatus_Error_Box = "fa fa-times-circle";
            this.serverStatus_Error_Message = this.serverResponse["EvertzInterviewApp"]["ParameterList"]["Reason"] + ", Please contact your Supervisor." ;
            // stop here if form is invalid
            if (this.registerForm.invalid) {
              return;
          }
        
        }
        console.log("Server Connected");
      },
      (err: HttpErrorResponse) => {     
        if(err.status == 0) {
          window.scrollTo(0, 0);
          console.log("Validation failed for Ip: " + this.serverIp)
          window.alert("Server Refused to connect, Please contact your Supervisor");
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server Refused to connect, Please contact your Supervisor."
          // stop here if form is invalid
          if (this.registerForm.invalid) {
            return;
        }
        }
        else {
          console.log("Validation successful for IP: " + this.serverIp)
        }
      }
    );

  };


  //Description: Function to save the Students Details by a WS-Call
  //Usage: To Save the details entered by the students to the DB by a post request
  //Arg's: Takes registerNumber, studentName, dateOfBirth, gender, college, degree, branch, yearOfPassing, CGPA, email, mobile, languageSelected as arguement
  public registerStudent(registerNumber, studentName, dateOfBirth, gender, college, degree, branch, yearOfPassing, CGPA, email, mobile, languageSelected){
    this.postToServerObject.saveStudentDetails(registerNumber, studentName, dateOfBirth, gender, college, degree, branch, yearOfPassing
      ,CGPA, email, mobile, languageSelected).subscribe((res: HttpResponse<any>) => {

            console.log("Saving Student detail: " + registerNumber)
            this.serverResponse = JSON.parse(JSON.stringify(res.body));
            console.log(res.body);
            this.registrationStatus = this.serverResponse["EvertzInterviewApp"]["Success"];
            if(!this.registrationStatus){
                  this.serverStatus_Error_Class = "isa_error";
                  this.serverStatus_Error_Box = "fa fa-times-circle";
                  this.serverStatus_Error_Message = this.serverResponse["EvertzInterviewApp"]["ParameterList"]["Reason"];
                  window.scrollTo(0, 0);
                  // stop here if form is invalid
                  if (this.registerForm.invalid) {
                    return;
                }
          }
      
          let navigationExtras: NavigationExtras = {
            queryParams: {
                registerNumber,
                studentName
            }
            
          };

          //generates Question for the candidate
          this.candiateGenerateQuestoins(registerNumber,this.questionType);
          //Moves to the next page, and passing registerNumber and languageName as arguement.
          this.router.navigate(['/questions'], navigationExtras);
      },
      (err: HttpErrorResponse) => {

        if(err.status == 0) {

          console.log("Server refused, Unable to save student detail: "+ registerNumber)
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server refused, Unable to save student detail: "+ registerNumber;
          window.scrollTo(0, 0);
              // stop here if form is invalid
              if (this.registerForm.invalid) {
                return;
            }
        }
        else {
          console.log("Student details saved sucessfully, Openning Questins page, for student: " + registerNumber)
        }

      }

    );

   };


   //Description: Function to Update the DropDown List in HTML UI
   //Usage: To update the college, branch, degree, Modules list on the UI
   //Arg's: Takes college, branch, degree, Modules as arguement which will be explicitly passed as requestList
  public getDropDownList(requestList) {

      var wsCallStatus = "";
      this.postToServerObject.getDropDownList(requestList).subscribe((res: HttpResponse<any>)=> {
      this.serverResponse = JSON.parse(JSON.stringify(res.body));
      wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];

      if(wsCallStatus){
        
        if(requestList == "College"){
            this.collegeName = this.serverResponse["EvertzInterviewApp"]["ParameterList"]["CollegeList"];
            console.log("college List Loaded Sucessfully...!");
        }
        else if(requestList == "Branch"){
            this.stream = this.serverResponse["EvertzInterviewApp"]["ParameterList"]["BranchList"];
            console.log("branch List Loaded Sucessfully...!");
        }
        else if(requestList == "Degree"){
          this.degree = this.serverResponse["EvertzInterviewApp"]["ParameterList"]["DegreeList"];
          console.log("degree List Loaded Sucessfully...!");
        }
        else if(requestList == "Modules"){
          this.programme = this.serverResponse["EvertzInterviewApp"]["ParameterList"]["ModuleList"];
          console.log("Modules List Loaded Sucessfully...!");
        }

      }
      else{

        this.serverStatus_Error_Class = "isa_error";
        this.serverStatus_Error_Box = "fa fa-times-circle";
        this.serverStatus_Error_Message = "Server Unable to load "+ requestList +" list, Please contact Your Supervisor."
        window.scrollTo(0, 0);
        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
         }
      }
      
    },
      (err: HttpErrorResponse) => { 

        if(err.status == 0) {
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server Unable to load "+ requestList +" list, Please contact Your Supervisor."
          window.scrollTo(0, 0);
          // stop here if form is invalid
          if (this.registerForm.invalid) {
              return;
           }
        }
        else {
          console.log("Sucessfully Updated: "+ Request + "List");
        }

      }

    );

  };


  //Description: This function will be called from HTML when all mandatory fields were updated
  //Usage: This function will save the students details to DB using WS-Call
  //Arg's: Takes registerNumber, studentName, dateOfBirth, gender, college, degree, branch, yearOfPassing, CGPA, email, mobile as arguement
  onSubmit(event, registerNumber, studentName, dateOfBirth, gender, college, degree, branch, yearOfPassing, CGPA, email, mobile) {

    this.submitted = true;

    if(gender == "" || college == "" || degree == "" || branch == "" || registerNumber == "" || studentName == "" || dateOfBirth == "" 
    || yearOfPassing == "" || CGPA == "" || email == "" || mobile == "" || this.languageSelected == ""){
        console.log("one of the required value is null");
        this.serverStatus_Error_Class = "isa_error";
        this.serverStatus_Error_Box = "fa fa-times-circle";
        this.serverStatus_Error_Message = "One of the required value value is not provided, Complete all required fields to start the Test."
        window.scrollTo(0, 0);

      // stop here if form is invalid
      if (this.registerForm.invalid) {
        return;
        }

    }

    this.loading = true;
    error => {
      this.error = error;
      this.loading = false;
    }

    this.validateServer(this.serverIp);
    this.registerStudent(registerNumber, studentName, dateOfBirth, gender, college, degree, branch, yearOfPassing, CGPA, email, mobile, this.languageSelected); 
    
  }
  


  public candiateGenerateQuestoins(registerNumber,questionType){

    this.postToServerObject.candiateGenerateQuestoins(registerNumber,questionType).subscribe((res: HttpResponse<any>)=> {
        console.log(JSON.parse(JSON.stringify(res.body)));
        var wsCallStatus = "";
        this.serverResponse = JSON.parse(JSON.stringify(res.body));
        wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];

        if(!wsCallStatus){

          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server Unable to Generate Questions list, Please contact Your Supervisor.";
          window.scrollTo(0, 0);

        }

      },
      (err: HttpErrorResponse) => {     
        if(err.status == 0) {
          window.scrollTo(0, 0);
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server failed to Generate Questions, Please contact your Supervisor.";
            // stop here if form is invalid
            if (this.registerForm.invalid) {
              return;
            }
          }
          else {
          console.log("Questions Generated sucessfully for : " + registerNumber);
        }
      }
    );
  };

}