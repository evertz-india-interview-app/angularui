import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CandidateRegistrationComponent } from './candidate-registration/candidate-registration.component';
import { QuestionsPageComponent } from './questions-page/questions-page.component';
import { AdminLoginComponent } from './admin-login/admin-login.component';
import { AdminControlComponent } from './admin-control/admin-control.component';
import { ResultsPageComponent } from './results-page/results-page.component';
import { AddQuestionsComponent } from './add-questions/add-questions.component';
import { CodingTestComponent } from './coding-test/coding-test.component';
import { AdminResetTestComponent } from './admin-reset-test/admin-reset-test.component';
import { AdminConfigAdditionComponent } from './admin-config-addition/admin-config-addition.component';


const routes: Routes = [
  { path: '', redirectTo: '/register', pathMatch: 'full' },
  { path: 'register', component: CandidateRegistrationComponent },
  { path: 'questions', component: QuestionsPageComponent },
  { path: 'admin', component:AdminLoginComponent},
  {path:'admincontrol', component:AdminControlComponent},
  {path:'results', component:ResultsPageComponent},
  {path:'addquestion', component:AddQuestionsComponent},
  {path:'coding', component:CodingTestComponent},
  {path:'reset', component:AdminResetTestComponent},
  {path:'config', component:AdminConfigAdditionComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
