import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse,HttpResponse } from '@angular/common/http';
import { PostToServerService } from '../post-to-server.service';
import {ActivatedRoute, Router, NavigationExtras} from "@angular/router";
import { SystemConfig } from '../ws-calls/system-config';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-coding-test',
  templateUrl: './coding-test.component.html',
  styleUrls: ['./coding-test.component.css']
})
export class CodingTestComponent implements OnInit {

  codeoutput:any;
  
  editorOptions = {theme: 'vs-dark', language: 'javascript',acceptSuggestionOnEnter:'off'};
  public registerNumber:any;
  public studentName:any;
  public codingLanguage:any;
  public questionsBlock: any;
  public questionToDisplay: any;
  public question: any;
  public isNextButtonEnaled = "false";
  public totalQuestoins: any;
  public nextQuestion = 0;
  public moduleName: any;
  public compilationStatus: any;
  public isTestCaseResultsAvailable = "false";
  public testCaseResults: any;
  public questionType = "Coding";
  public serverResponse = "";
  public serverStatus_Error_Class = "";
  public serverStatus_Error_Box = "";
  public serverStatus_Error_Message = "";

  constructor(private postToServerObject: PostToServerService,private route: ActivatedRoute,private systemConfig: SystemConfig) { 
      this.route.queryParams.subscribe(params => {
        this.registerNumber = params["registerNumber"];
        this.studentName = params["studentName"];           
    });
  }

  ngOnInit() {
    this.candiateGetQuestoins(this.registerNumber,this.questionType);
    //this.questionsBlock = this.systemConfig.questionsList["EvertzInterviewApp"]["ParameterList"]["QuestionsList"]["Questions"];
    //this.getQuestions();
  }

  getQuestions(){
    this.totalQuestoins = this.questionsBlock.length;
    if(this.questionsBlock.length > 1){
      this.questionToDisplay = this.questionsBlock[0];
      this.question = this.questionToDisplay["Question"];
      this.moduleName = this.questionToDisplay["ModuleName"];
    }
  }

  saveProgram(code){
    //this.candidateCode = code;
    console.log(code._value);
    this.compilationStatus = "";
    this.isTestCaseResultsAvailable = "false";
    this.candiateSaveAnswer(this.registerNumber,this.questionToDisplay["QId"],code._value,this.questionType);
  }

  compileProgram(code){

    this.isTestCaseResultsAvailable = "false";
    this.compilationStatus = "";
    this.sequence_Compile_1(this.registerNumber,this.questionToDisplay["QId"],code._value,this.questionType);

  }

  runProgram(code){

    this.isTestCaseResultsAvailable = "false";
    this.compilationStatus = "";
    this.sequence_Run_1(this.registerNumber,this.questionToDisplay["QId"],code._value,this.questionType);

  }

  submitProgram(code){

    this.isTestCaseResultsAvailable = "false";
    this.compilationStatus = "";
    this.candiateSubmitAnswer(this.registerNumber,this.questionToDisplay["QId"],code._value,this.questionType);

  }

  nextProgram(code){
    
    this.nextQuestion += 1;
    this.questionToDisplay = this.questionsBlock[this.nextQuestion];
    this.question = this.questionToDisplay["Question"];
    this.moduleName = this.questionToDisplay["ModuleName"];
    this.isTestCaseResultsAvailable = "false";
    this.compilationStatus = "";
    if(this.totalQuestoins == this.nextQuestion){
      this.isNextButtonEnaled = "false";
    }

  }

  validateTestCases() {
    this.isTestCaseResultsAvailable = "true";
  }

  public candiateGetQuestoins(registerNumber,questionType){

    this.postToServerObject.candiateGetQuestoins(registerNumber,questionType).subscribe((res: HttpResponse<any>)=> {
        console.log(JSON.parse(JSON.stringify(res.body)));
        var wsCallStatus = "";
        this.serverResponse = JSON.parse(JSON.stringify(res.body));
        wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];

        if(!wsCallStatus){

          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server Unable to Get Coding Questions list, Please contact Your Supervisor.";
          window.scrollTo(0, 0);
          window.stop();
        }

        this.questionsBlock = this.serverResponse["EvertzInterviewApp"]["ParameterList"]["QuestionsList"]["Questions"];
        console.log(this.questionsBlock);
        this.totalQuestoins = this.questionsBlock.length;

        if(this.questionsBlock.length > 1){
          this.questionToDisplay = this.questionsBlock[0];
          this.question = this.questionToDisplay["Question"];
          this.moduleName = this.questionToDisplay["ModuleName"];
          this.isNextButtonEnaled = "true";
        }else{

          this.questionToDisplay = this.questionsBlock[0];
          this.question = this.questionToDisplay["Question"];
          this.moduleName = this.questionToDisplay["ModuleName"];

        }

      },
      (err: HttpErrorResponse) => {     
        if(err.status == 0) {

          window.scrollTo(0, 0);
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server failed to Get Coding Questions List, Please contact your Supervisor.";

          }
          else {
          console.log("Questions Received sucessfully for : " + registerNumber);
        }
      }
    );
  };

  public candiateSaveAnswer(registerNumber,qId,answer,option){

    this.postToServerObject.candiateSaveAnswer(registerNumber,qId,answer,option).subscribe((res: HttpResponse<any>)=> {
        var wsCallStatus = "";
        this.serverResponse = JSON.parse(JSON.stringify(res.body));
        console.log(this.serverResponse);
        wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];

        if(!wsCallStatus){

          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server Unable to Save Answer, Please contact Your Supervisor.";
          window.scrollTo(0, 0);

        }
        console.log("program saved to DB");

      },
      (err: HttpErrorResponse) => {     
        if(err.status == 0) {

          window.scrollTo(0, 0);
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Failed to Save Answer, please resubmit your answer.";

          }
          else {
          console.log("Question answer saved to DB sucessfully : " + qId);
        }
      }
    );
  };

  public candiateSubmitAnswer(registerNumber,qId,answer,option){

    this.postToServerObject.candiateSubmitAnswer(registerNumber,qId,answer,option).subscribe((res: HttpResponse<any>)=> {
        console.log(JSON.parse(JSON.stringify(res.body)));
        var wsCallStatus = "";
        this.serverResponse = JSON.parse(JSON.stringify(res.body));
        wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];

        if(!wsCallStatus){

          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Failed to Submit Test, Please contact your Supervisor.";
          window.scrollTo(0, 0);
          window.stop();
        }

        console.log("Test Submitted Sucessfully...")
      },
      (err: HttpErrorResponse) => {     
        if(err.status == 0) {

          window.scrollTo(0, 0);
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Failed to Submit Test, Please contact your Supervisor.";

          }
          else {
          console.log("Test submitted sucessfully for Student: " + registerNumber);
        }
      }
    );
  };

  public sequence_Compile_1(registerNumber,qId,answer,option){

    this.postToServerObject.candiateSaveAnswer(registerNumber,qId,answer,option).subscribe((res: HttpResponse<any>)=> {
        var wsCallStatus = "";
        this.serverResponse = JSON.parse(JSON.stringify(res.body));
        console.log(this.serverResponse);
        wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];

        if(!wsCallStatus){

          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server Unable to Save Answer, Please contact Your Supervisor.";
          window.scrollTo(0, 0);

        }
        console.log("program saved to DB, compiling...");
        this.sequence_Compile_2("CompileProgram",registerNumber,qId,option)
      },
      (err: HttpErrorResponse) => {     
        if(err.status == 0) {

          window.scrollTo(0, 0);
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Failed to Save Answer, please resubmit your answer.";

          }
          else {
          console.log("Question answer saved to DB sucessfully, Compiling... : " + qId);
        }
      }
    );
  };

  public sequence_Compile_2(command,registerNumber,questionId,questionType){

    this.postToServerObject.candiateCodingTask(command,registerNumber,questionId,questionType).subscribe((res: HttpResponse<any>)=> {
        console.log(JSON.parse(JSON.stringify(res.body)));
        var wsCallStatus = "";
        this.serverResponse = JSON.parse(JSON.stringify(res.body));
        wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];

        if(!wsCallStatus){

          /*this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Unable to "+ command +", Please contact Your Supervisor.";*/
          this.compilationStatus = this.serverResponse["EvertzInterviewApp"]["Reason"];
          window.scrollTo(0, 0);
          window.stop();
        }
        console.log("Program Compiled sucessfully.");
        this.compilationStatus = "Program Compiled sucessfully.";
      },
      (err: HttpErrorResponse) => {     
        if(err.status == 0) {

          window.scrollTo(0, 0);
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server failed to "+ command +", Please contact your Supervisor.";

          }
          else {
          console.log("Program Compiled sucessfully.: " + questionId);
        }
      }
    );
  };

  public sequence_Run_1(registerNumber,qId,answer,option){

    this.postToServerObject.candiateSaveAnswer(registerNumber,qId,answer,option).subscribe((res: HttpResponse<any>)=> {
        var wsCallStatus = "";
        this.serverResponse = JSON.parse(JSON.stringify(res.body));
        console.log(this.serverResponse);
        wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];

        if(!wsCallStatus){

          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server Unable to Save Answer, Please contact Your Supervisor.";
          window.scrollTo(0, 0);

        }
        console.log("program saved to DB, compiling...");
        this.sequence_Run_2("CompileProgram",registerNumber,qId,option)
      },
      (err: HttpErrorResponse) => {     
        if(err.status == 0) {

          window.scrollTo(0, 0);
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Failed to Save Answer, please resubmit your answer.";

          }
          else {
          console.log("Question answer saved to DB sucessfully, Compiling... : " + qId);
        }
      }
    );
  };

  public sequence_Run_2(command,registerNumber,questionId,questionType){

    this.postToServerObject.candiateCodingTask(command,registerNumber,questionId,questionType).subscribe((res: HttpResponse<any>)=> {
        console.log(JSON.parse(JSON.stringify(res.body)));
        var wsCallStatus = "";
        this.serverResponse = JSON.parse(JSON.stringify(res.body));
        wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];

        if(!wsCallStatus){

          /*this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Unable to "+ command +", Please contact Your Supervisor.";*/
          this.compilationStatus = this.serverResponse["EvertzInterviewApp"]["Reason"];
          window.scrollTo(0, 0);
          window.stop();
        }
        console.log("Program Compiled Sucessfully, Running... ");
        this.compilationStatus = "Program Compiled Sucessfully."
        this.sequence_Run_3("RunProgram",registerNumber,questionId,questionType)
      },
      (err: HttpErrorResponse) => {     
        if(err.status == 0) {

          window.scrollTo(0, 0);
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server failed to "+ command +", Please contact your Supervisor.";

          }
          else {
            console.log("Program Compiled Sucessfully, Running... : " + questionId);
        }
      }
    );
  };

  public sequence_Run_3(command,registerNumber,questionId,questionType){

    this.postToServerObject.candiateCodingTask(command,registerNumber,questionId,questionType).subscribe((res: HttpResponse<any>)=> {
        console.log(JSON.parse(JSON.stringify(res.body)));
        var wsCallStatus = "";
        this.serverResponse = JSON.parse(JSON.stringify(res.body));
        wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];

        if(!wsCallStatus){

          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Unable to "+ command +", Please contact Your Supervisor.";
          window.scrollTo(0, 0);
          window.stop();
        }
        console.log("Program Ran Sucessfully...");
        this.testCaseResults = this.serverResponse["EvertzInterviewApp"]["ParameterList"]["TestCaseList"];
        console.log("Validating Results...");
        this.validateTestCases();
      },
      (err: HttpErrorResponse) => {     
        if(err.status == 0) {

          window.scrollTo(0, 0);
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server failed to "+ command +", Please contact your Supervisor.";

          }
          else {
            console.log("Program Ran Sucessfully for : " + questionId);
        }
      }
    );
  };

}
