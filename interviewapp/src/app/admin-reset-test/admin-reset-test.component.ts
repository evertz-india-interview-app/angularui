import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PostToServerService } from '../post-to-server.service';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-admin-reset-test',
  templateUrl: './admin-reset-test.component.html',
  styleUrls: ['./admin-reset-test.component.css']
})
export class AdminResetTestComponent {
  
  public serverResponse: any;
  public serverStatus_Error_Class = "";
  public serverStatus_Error_Box = "";
  public serverStatus_Error_Message = "";
  public questionTypeList = ['All','MCQ','Coding'];
  public questionType: any;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver,private postToServerObject: PostToServerService,) {}

  resetTest(regno){
    console.log(regno);
  }

  public adminResetTest(registerNumber,questionType){

    this.postToServerObject.adminResetTest(registerNumber,questionType).subscribe((res: HttpResponse<any>)=> {
        console.log(JSON.parse(JSON.stringify(res.body)));
        var wsCallStatus = "";
        this.serverResponse = JSON.parse(JSON.stringify(res.body));
        wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];

        if(!wsCallStatus){

          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Failed to rest";
          window.scrollTo(0, 0);
          window.stop();
        }

      },
      (err: HttpErrorResponse) => {     
        if(err.status == 0) {

          window.scrollTo(0, 0);
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server failed to Get Coding Questions List, Please contact your Supervisor.";

          }
          else {
          console.log("Questions Received sucessfully for : " + registerNumber);
        }
      }
    );
  };

}
