import { Component, OnInit, ViewChild } from '@angular/core';
import {ActivatedRoute, Router, NavigationExtras} from "@angular/router";
import { SystemConfig } from '../ws-calls/system-config';
import { PostToServerService } from '../post-to-server.service';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { timer } from 'rxjs';
import { CountdownComponent } from 'ngx-countdown';

@Component({
  selector: 'app-questions-page',
  templateUrl: './questions-page.component.html',
  styleUrls: ['./questions-page.component.css']
})
export class QuestionsPageComponent implements OnInit {

  public registerNumber: string;
  public studentName: string;
  public previousQid: any;
  public passButtonClass = "success";
  public defaultButtonClass = "outline-primary";
  public questionType = "MCQ";
  public serverResponse = "";
  public serverStatus_Error_Class = "";
  public serverStatus_Error_Box = "";
  public serverStatus_Error_Message = "";
  //public questionJson = this.systemConfig.questionsList["EvertzInterviewApp"]["Output"]["ParameterList"]["QuestionsList"]
  public questionJson: any;
  public moduleOnHTML = "";
  public questionOnHTML = "";
  public optionIdSelected = "";
  public optionIdSelectedList = [];
  public optionIdSelectdByCandidate = "";
  public buttonColor = [];
  public questionBlock = [];
  public moduleName = [];
  public questionToDisplay = [];
  public sortedModuleQuestions = [];
  public unsortedModuleQuestions = [];
  public totalQuestions: any;
  public isLastQuestion = "false";
  public isQuestionsLoaded = "false";
  public submitted = false;

  seconds: number = 60;
  minutes: number = 59;
  hours: number = 0;
  interval;


  @ViewChild('countdown') counter:
  CountdownComponent;

  
  finishTest(){
    this.candiateSubmitAnswer(this.registerNumber,this.questionToDisplay["QId"],this.optionIdSelected,this.questionType);
  }

  resetTimer(){
    this.counter.restart();
  }

   
  //To Update the Registration Number and Language Selected from Registration Page
  public constructor(private postToServerObject: PostToServerService,private route: ActivatedRoute, private systemConfig: SystemConfig,private router: Router,private loadingBar: LoadingBarService) {
      this.route.queryParams.subscribe(params => {
          this.registerNumber = params["registerNumber"];
          this.studentName = params["studentName"];           
      });
    }

ngOnInit() {

    this.startLoading();
    this.candiateGetQuestoins(this.registerNumber,this.questionType);
   // this.startTimer();
  }

  /*startTimer() {
    this.interval = setInterval(() => {
      if(this.seconds > 0) {
        this.seconds--;
      } else {
        this.seconds = 60;
        this.minutes--;
        if(this.minutes == 0){
          this.hours--;
          console.log("Time Out");
        }
      }
    },1000)
  }*/

  startLoading() {
    this.loadingBar.start();
  }

  stopLoading() {
    this.loadingBar.complete();
  }

  getQuestionsUpdateToUI(){

        //console.log(this.questionJson);
        //iterate to get QuestionsList
        for(let question in this.questionJson){
          this.questionBlock[question] = this.questionJson[question];
          this.buttonColor.push(this.defaultButtonClass);
          this.optionIdSelectedList.push("0");
        }

      //iterate to find how many Modules in questions list
      for(let module in this.questionBlock){
        var moduleExists = this.moduleName.includes(this.questionBlock[module]["ModuleName"]);
        if(!moduleExists){
          this.moduleName.push(this.questionBlock[module]["ModuleName"]);
        }
      }

      //to seperate out the questions based on ModuleName
      for(let module in this.moduleName){
        for(let moduleInjson in this.questionBlock){
            if(this.moduleName[module] == this.questionBlock[moduleInjson]["ModuleName"]){
                var bindModule = this.questionBlock[moduleInjson]["Questions"][0]["Question"];
                bindModule = this.questionBlock[moduleInjson]["ModuleName"] + "|" + bindModule;
                this.questionBlock[moduleInjson]["Questions"][0]["Question"] = bindModule;
                this.unsortedModuleQuestions.push(this.questionBlock[moduleInjson]["Questions"][0]);
            }
        }
        this.sortQuestions();
        this.unsortedModuleQuestions = [];

      }

      //to check the total questions
      console.log(this.moduleName);
      console.log(this.sortedModuleQuestions);
      //console.log(this.sortedModuleQuestions[0]);

      //setting Question1 on load
      this.questionToDisplay = [];
      this.questionToDisplay = this.sortedModuleQuestions[0];
      var moduleInSelectedQuestion = this.questionToDisplay["Question"].split("|")[0];
      this.questionOnHTML = this.questionToDisplay["Question"].substring(moduleInSelectedQuestion.length+1);
      this.previousQid = 0;

      this.stopLoading();
      this.isQuestionsLoaded = "true";
  }

  sortQuestions(){

    for(var i=0; i<this.unsortedModuleQuestions.length; i++){
      for(var j=i+1; j<this.unsortedModuleQuestions.length; j++){
          if( parseInt( this.unsortedModuleQuestions[i]["Id"]) > parseInt (this.unsortedModuleQuestions[j]["Id"]) ){
            var temp = this.unsortedModuleQuestions[i]["Id"];
            this.unsortedModuleQuestions[i]["Id"] = this.unsortedModuleQuestions[j]["Id"];
            this.unsortedModuleQuestions[j]["Id"] = temp;
          }
      }
    }

    for(let questions in this.unsortedModuleQuestions){
        this.sortedModuleQuestions.push(this.unsortedModuleQuestions[questions]);
    }
    
    this.totalQuestions = this.sortedModuleQuestions.length;
  }

  
  checkModuleQuestion(totalQuestions){
    if(totalQuestions["Question"].split("|")[0] == this.moduleOnHTML){
      return true;
    }else{
      return false;
    }
    
  }

  checkModule(moduleOnUI){
    this.moduleOnHTML = moduleOnUI.toString();
    return true;
  }

  displayQuestionOnUI(id){

    this.questionToDisplay = [];

    this.questionToDisplay = this.sortedModuleQuestions[parseInt(id)-1];
    var moduleInSelectedQuestion = this.questionToDisplay["Question"].split("|")[0];
    this.questionOnHTML = this.questionToDisplay["Question"].substring(moduleInSelectedQuestion.length+1);
    this.optionIdSelectdByCandidate = this.optionIdSelectedList[parseInt(id)];
    if(this.totalQuestions == parseInt(this.questionToDisplay["Id"])){
      this.isLastQuestion = "true";
    }else{
      this.isLastQuestion = "false";
    }
    //console.log(id-1);
    //console.log(this.previousQid);

    console.log("Id clicked: "+this.questionToDisplay["QId"]);

    if(this.previousQid != id-1){
      if(this.buttonColor[this.previousQid] == this.passButtonClass){
      }else{
        this.buttonColor[this.previousQid] = this.defaultButtonClass;
      }      
    }

    this.previousQid = id-1;

  }

  radioChangeHandler(event: any){
    this.optionIdSelected = event.target.value;
  }

  /*saveQuestionClicked(){

    if(this.optionIdSelected!=""){
    this.buttonColor[parseInt(this.questionToDisplay["Id"])-1] = this.passButtonClass;
    this.optionIdSelectedList[parseInt(this.questionToDisplay["Id"])] = this.optionIdSelected;
    }
    this.previousQid = parseInt(this.questionToDisplay["Id"])-1;
    this.optionIdSelected = "";

  }*/

  nextQuestionClicked(){

    if(this.isLastQuestion !="true"){

      console.log("Processing ID: "+this.questionToDisplay["QId"]);

        //saves the option and moves to the next question
        if(this.optionIdSelected!=""){
          this.buttonColor[parseInt(this.questionToDisplay["Id"])-1] = this.passButtonClass;
          this.optionIdSelectedList[parseInt(this.questionToDisplay["Id"])] = this.optionIdSelected;
          this.candiateSaveAnswer(this.registerNumber,this.questionToDisplay["QId"],this.optionIdSelected,this.questionType);
        }

        this.optionIdSelected = "";
        
        this.previousQid = parseInt(this.questionToDisplay["Id"]);

        this.questionToDisplay = this.sortedModuleQuestions[parseInt(this.questionToDisplay["Id"])];

        var moduleInSelectedQuestion = this.questionToDisplay["Question"].split("|")[0];
        this.questionOnHTML = this.questionToDisplay["Question"].substring(moduleInSelectedQuestion.length+1);

        //for button changes
        if(this.buttonColor[parseInt(this.questionToDisplay["Id"])-2] != this.passButtonClass){
          this.buttonColor[parseInt(this.questionToDisplay["Id"])-2] = this.defaultButtonClass;
        }

        //checking if moduleId is the last question
        if((this.totalQuestions-1 == parseInt(this.questionToDisplay["Id"])-1) || (this.totalQuestions == parseInt(this.questionToDisplay["Id"]))){
          console.log("last question in module");
          this.isLastQuestion = "true";
        }
    }else{

      if(this.optionIdSelected!=""){
        this.buttonColor[parseInt(this.questionToDisplay["Id"])-1] = this.passButtonClass;
        this.optionIdSelectedList[parseInt(this.questionToDisplay["Id"])] = this.optionIdSelected;
      }
      //saving the Answer to DB
      console.log("Processing ID: "+this.questionToDisplay["QId"]);

      console.log("calling submit Test function");
      this.candiateSubmitAnswer(this.registerNumber,this.questionToDisplay["QId"],this.optionIdSelected,this.questionType);
    }

  }

    onSubmit(event:any) {
      this.submitted = true;
      //console.log("Submitted Successfully");
      $('#submitPrompt').modal('hide');
      /*let navigationExtras: NavigationExtras = {
        queryParams: {
            registerNumber : this.registerNumber,
            studentName : this.studentName
        }
        
      };*/
      

      this.candiateSubmitAnswer(this.registerNumber,this.questionToDisplay["QId"],this.optionIdSelected,this.questionType);

      //Moves to the next page, and passing registerNumber and languageName as arguement.
      //this.router.navigate(['/coding'], navigationExtras);
  }

  public candiateGetQuestoins(registerNumber,questionType){

    this.postToServerObject.candiateGetQuestoins(registerNumber,questionType).subscribe((res: HttpResponse<any>)=> {
        console.log(JSON.parse(JSON.stringify(res.body)));
        var wsCallStatus = "";
        this.serverResponse = JSON.parse(JSON.stringify(res.body));
        wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];

        if(!wsCallStatus){

          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server Unable to Get Questions list, Please contact Your Supervisor.";
          window.scrollTo(0, 0);
          window.stop();
        }

        this.questionJson = this.serverResponse["EvertzInterviewApp"]["ParameterList"]["QuestionsList"];
        //console.log(this.questionJson);
        //console.log("Questoins Added");
        this.getQuestionsUpdateToUI();

      },
      (err: HttpErrorResponse) => {     
        if(err.status == 0) {

          window.scrollTo(0, 0);
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server failed to Get Questions List, Please contact your Supervisor.";

          }
          else {
          console.log("Questions Received sucessfully for : " + registerNumber);
        }
      }
    );
  };


  public candiateSaveAnswer(registerNumber,qId,answer,option){

    this.postToServerObject.candiateSaveAnswer(registerNumber,qId,answer,option).subscribe((res: HttpResponse<any>)=> {
        var wsCallStatus = "";
        this.serverResponse = JSON.parse(JSON.stringify(res.body));
        console.log(this.serverResponse);
        wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];

        if(!wsCallStatus){

          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server Unable to Save Answer, Please contact Your Supervisor.";
          window.scrollTo(0, 0);

        }

      },
      (err: HttpErrorResponse) => {     
        if(err.status == 0) {

          window.scrollTo(0, 0);
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Failed to Save Answer, please resubmit your answer.";

          }
          else {
          console.log("Question answer saved to DB sucessfully : " + qId);
        }
      }
    );
  };

  public candiateSubmitAnswer(registerNumber,qId,answer,option){

    this.postToServerObject.candiateSubmitAnswer(registerNumber,qId,answer,option).subscribe((res: HttpResponse<any>)=> {
        console.log(JSON.parse(JSON.stringify(res.body)));
        var wsCallStatus = "";
        this.serverResponse = JSON.parse(JSON.stringify(res.body));
        wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];

        if(!wsCallStatus){

          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Failed to Submit Test, Please contact your Supervisor.";
          window.scrollTo(0, 0);
          window.stop();
        }

        //call generate questions for coding
        option = "Coding";
        this.candiateGenerateQuestoins(registerNumber,option);

      },
      (err: HttpErrorResponse) => {     
        if(err.status == 0) {

          window.scrollTo(0, 0);
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Failed to Submit Test, Please contact your Supervisor.";

          }
          else {
          console.log("Test submitted sucessfully for Student: " + registerNumber);
        }
      }
    );
  };

  public candiateGenerateQuestoins(registerNumber,questionType){

    this.postToServerObject.candiateGenerateQuestoins(registerNumber,questionType).subscribe((res: HttpResponse<any>)=> {
        console.log(JSON.parse(JSON.stringify(res.body)));
        var wsCallStatus = "";
        this.serverResponse = JSON.parse(JSON.stringify(res.body));
        wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];

        if(!wsCallStatus){

          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server Unable to Generate Coding Questions list, Please contact Your Supervisor.";
          window.scrollTo(0, 0);
          window.stop();
        }
        let navigationExtras: NavigationExtras = {
          queryParams: {
              registerNumber : this.registerNumber,
              studentName : this.studentName
          }
          
        };

        console.log("Coding Questions Generated sucessfully for : " + registerNumber);
        
        //Moves to the next page, and passing registerNumber and languageName as arguement.
        this.router.navigate(['/coding'], navigationExtras);

      },
      (err: HttpErrorResponse) => {     
        if(err.status == 0) {
          window.scrollTo(0, 0);
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server failed to Generate Coding Questions, Please contact your Supervisor.";

          }
          else {
          console.log("Coding Questions Generated sucessfully for : " + registerNumber);
        }
      }
    );
  };

}