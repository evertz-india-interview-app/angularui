import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

@Component({
  selector: 'app-add-questions',
  templateUrl: './add-questions.component.html',
  styleUrls: ['./add-questions.component.css']
})
export class AddQuestionsComponent {

  title = 'ADDQUESTION';
  Module = ["Java", "Javascript", "C", "C++", "Python"];
  public Answer :any = ["1","2"];
  public addQuestion:boolean = false;
  public buttonName:any = 'addQuestion';
  public Expand:boolean = false;
  public buttonName1:any = '+';
  Option1: string;
  Option2: string;
  Option3: string;
  Option4: string;
  Questiondata: string;
  AnswerValue: string;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver) {}

  toggle() {
    
    this.Expand = !this.Expand;
    
    // CHANGE THE NAME OF THE BUTTON.
    if(this.Expand)  
    {
      this.buttonName1 = "-";
      this.Answer.push("3","4")
      console.log(this.Answer);
    }
    else
     {
      this.Answer.splice(2,2);
      this.buttonName1 = "+";
      
      console.log(this.Answer)
     }
     
  }

  handleClick(event: Event) {
    console.log("Click!", event)
    console.log(this.Questiondata,this.Option1,this.Option2,this.Option3,this.Option4,this.AnswerValue,this.selectedModule,this.selectedAnswer);
  }

  
  selectedModule: string = '';
  selectChangeHandler (event: any) {
    this.selectedModule = event.target.value;
  }


  selectedAnswer: string = '';
  selectAnswerHandler(event: any) {
    this.selectedAnswer = event.target.value;
  }

}
