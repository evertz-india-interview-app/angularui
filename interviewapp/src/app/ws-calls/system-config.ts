import { Injectable } from '@angular/core';

@Injectable()
export class SystemConfig {

    constructor() {}

    public serverIp = "10.42.10.21";

    public questionsList = {
        "EvertzInterviewApp": {
            "Command": "GetQuestions",
            "Subsystem": "Candidate",
            "ParameterList": {
                "RegisterNumber": "789656",
                "QuestionsList": {
                    "Questions": [
                        {
                            "ModuleName": "Java",
                            "Question": "Program Java 8",
                            "QId": "66"
                        },
                        {
                            "ModuleName": "Java",
                            "Question": "Program Java 9",
                            "QId": "67"
                        },
                        {
                            "ModuleName": "Java",
                            "Question": "Program Java 10",
                            "QId": "68"
                        }
                    ]
                }
            },
            "Success": true
        }
    }
}