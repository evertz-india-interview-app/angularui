(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/forms'), require('rxjs'), require('@angular/common')) :
    typeof define === 'function' && define.amd ? define('ngx-monaco-editor', ['exports', '@angular/core', '@angular/forms', 'rxjs', '@angular/common'], factory) :
    (global = global || self, factory(global['ngx-monaco-editor'] = {}, global.ng.core, global.ng.forms, global.rxjs, global.ng.common));
}(this, function (exports, core, forms, rxjs, common) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __exportStar(m, exports) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
        if (m) return m.call(o);
        return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var loadedMonaco = false;
    /** @type {?} */
    var loadPromise;
    /**
     * @abstract
     */
    var BaseEditor = /** @class */ (function () {
        function BaseEditor(config) {
            this.config = config;
            this.onInit = new core.EventEmitter();
        }
        Object.defineProperty(BaseEditor.prototype, "options", {
            get: /**
             * @return {?}
             */
            function () {
                return this._options;
            },
            set: /**
             * @param {?} options
             * @return {?}
             */
            function (options) {
                this._options = Object.assign({}, this.config.defaultOptions, options);
                if (this._editor) {
                    this._editor.dispose();
                    this.initMonaco(options);
                }
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @return {?}
         */
        BaseEditor.prototype.ngAfterViewInit = /**
         * @return {?}
         */
        function () {
            var _this = this;
            if (loadedMonaco) {
                // Wait until monaco editor is available
                loadPromise.then((/**
                 * @return {?}
                 */
                function () {
                    _this.initMonaco(_this.options);
                }));
            }
            else {
                loadedMonaco = true;
                loadPromise = new Promise((/**
                 * @param {?} resolve
                 * @return {?}
                 */
                function (resolve) {
                    /** @type {?} */
                    var baseUrl = _this.config.baseUrl || './assets';
                    if (typeof (((/** @type {?} */ (window))).monaco) === 'object') {
                        resolve();
                        return;
                    }
                    /** @type {?} */
                    var onGotAmdLoader = (/**
                     * @return {?}
                     */
                    function () {
                        // Load monaco
                        ((/** @type {?} */ (window))).require.config({ paths: { 'vs': baseUrl + "/monaco/vs" } });
                        ((/** @type {?} */ (window))).require(['vs/editor/editor.main'], (/**
                         * @return {?}
                         */
                        function () {
                            if (typeof _this.config.onMonacoLoad === 'function') {
                                _this.config.onMonacoLoad();
                            }
                            _this.initMonaco(_this.options);
                            resolve();
                        }));
                    });
                    // Load AMD loader if necessary
                    if (!((/** @type {?} */ (window))).require) {
                        /** @type {?} */
                        var loaderScript = document.createElement('script');
                        loaderScript.type = 'text/javascript';
                        loaderScript.src = baseUrl + "/monaco/vs/loader.js";
                        loaderScript.addEventListener('load', onGotAmdLoader);
                        document.body.appendChild(loaderScript);
                    }
                    else {
                        onGotAmdLoader();
                    }
                }));
            }
        };
        /**
         * @return {?}
         */
        BaseEditor.prototype.ngOnDestroy = /**
         * @return {?}
         */
        function () {
            if (this._windowResizeSubscription) {
                this._windowResizeSubscription.unsubscribe();
            }
            if (this._editor) {
                this._editor.dispose();
                this._editor = undefined;
            }
        };
        BaseEditor.propDecorators = {
            _editorContainer: [{ type: core.ViewChild, args: ['editorContainer', { static: true },] }],
            onInit: [{ type: core.Output }],
            options: [{ type: core.Input, args: ['options',] }]
        };
        return BaseEditor;
    }());
    if (false) {
        /** @type {?} */
        BaseEditor.prototype._editorContainer;
        /** @type {?} */
        BaseEditor.prototype.onInit;
        /**
         * @type {?}
         * @protected
         */
        BaseEditor.prototype._editor;
        /**
         * @type {?}
         * @private
         */
        BaseEditor.prototype._options;
        /**
         * @type {?}
         * @protected
         */
        BaseEditor.prototype._windowResizeSubscription;
        /**
         * @type {?}
         * @private
         */
        BaseEditor.prototype.config;
        /**
         * @abstract
         * @protected
         * @param {?} options
         * @return {?}
         */
        BaseEditor.prototype.initMonaco = function (options) { };
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var NGX_MONACO_EDITOR_CONFIG = new core.InjectionToken('NGX_MONACO_EDITOR_CONFIG');
    /**
     * @record
     */
    function NgxMonacoEditorConfig() { }
    if (false) {
        /** @type {?|undefined} */
        NgxMonacoEditorConfig.prototype.baseUrl;
        /** @type {?|undefined} */
        NgxMonacoEditorConfig.prototype.defaultOptions;
        /** @type {?|undefined} */
        NgxMonacoEditorConfig.prototype.onMonacoLoad;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var EditorComponent = /** @class */ (function (_super) {
        __extends(EditorComponent, _super);
        function EditorComponent(zone, editorConfig) {
            var _this = _super.call(this, editorConfig) || this;
            _this.zone = zone;
            _this.editorConfig = editorConfig;
            _this._value = '';
            _this.propagateChange = (/**
             * @param {?} _
             * @return {?}
             */
            function (_) { });
            _this.onTouched = (/**
             * @return {?}
             */
            function () { });
            return _this;
        }
        Object.defineProperty(EditorComponent.prototype, "model", {
            set: /**
             * @param {?} model
             * @return {?}
             */
            function (model) {
                this.options.model = model;
                if (this._editor) {
                    this._editor.dispose();
                    this.initMonaco(this.options);
                }
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} value
         * @return {?}
         */
        EditorComponent.prototype.writeValue = /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            var _this = this;
            this._value = value || '';
            // Fix for value change while dispose in process.
            setTimeout((/**
             * @return {?}
             */
            function () {
                if (_this._editor && !_this.options.model) {
                    _this._editor.setValue(_this._value);
                }
            }));
        };
        /**
         * @param {?} fn
         * @return {?}
         */
        EditorComponent.prototype.registerOnChange = /**
         * @param {?} fn
         * @return {?}
         */
        function (fn) {
            this.propagateChange = fn;
        };
        /**
         * @param {?} fn
         * @return {?}
         */
        EditorComponent.prototype.registerOnTouched = /**
         * @param {?} fn
         * @return {?}
         */
        function (fn) {
            this.onTouched = fn;
        };
        /**
         * @protected
         * @param {?} options
         * @return {?}
         */
        EditorComponent.prototype.initMonaco = /**
         * @protected
         * @param {?} options
         * @return {?}
         */
        function (options) {
            var _this = this;
            /** @type {?} */
            var hasModel = !!options.model;
            if (hasModel) {
                /** @type {?} */
                var model = monaco.editor.getModel(options.model.uri || '');
                if (model) {
                    options.model = model;
                    options.model.setValue(this._value);
                }
                else {
                    options.model = monaco.editor.createModel(options.model.value, options.model.language, options.model.uri);
                }
            }
            this._editor = monaco.editor.create(this._editorContainer.nativeElement, options);
            if (!hasModel) {
                this._editor.setValue(this._value);
            }
            this._editor.onDidChangeModelContent((/**
             * @param {?} e
             * @return {?}
             */
            function (e) {
                /** @type {?} */
                var value = _this._editor.getValue();
                _this.propagateChange(value);
                // value is not propagated to parent when executing outside zone.
                _this.zone.run((/**
                 * @return {?}
                 */
                function () { return _this._value = value; }));
            }));
            this._editor.onDidBlurEditorWidget((/**
             * @return {?}
             */
            function () {
                _this.onTouched();
            }));
            // refresh layout on resize event.
            if (this._windowResizeSubscription) {
                this._windowResizeSubscription.unsubscribe();
            }
            this._windowResizeSubscription = rxjs.fromEvent(window, 'resize').subscribe((/**
             * @return {?}
             */
            function () { return _this._editor.layout(); }));
            this.onInit.emit(this._editor);
        };
        EditorComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'ngx-monaco-editor',
                        template: '<div class="editor-container" #editorContainer></div>',
                        providers: [{
                                provide: forms.NG_VALUE_ACCESSOR,
                                useExisting: core.forwardRef((/**
                                 * @return {?}
                                 */
                                function () { return EditorComponent; })),
                                multi: true
                            }],
                        styles: ["\n    :host {\n      display: block;\n      height: 200px;\n    }\n\n    .editor-container {\n      width: 100%;\n      height: 98%;\n    }\n  "]
                    }] }
        ];
        /** @nocollapse */
        EditorComponent.ctorParameters = function () { return [
            { type: core.NgZone },
            { type: undefined, decorators: [{ type: core.Inject, args: [NGX_MONACO_EDITOR_CONFIG,] }] }
        ]; };
        EditorComponent.propDecorators = {
            model: [{ type: core.Input, args: ['model',] }]
        };
        return EditorComponent;
    }(BaseEditor));
    if (false) {
        /**
         * @type {?}
         * @private
         */
        EditorComponent.prototype._value;
        /** @type {?} */
        EditorComponent.prototype.propagateChange;
        /** @type {?} */
        EditorComponent.prototype.onTouched;
        /**
         * @type {?}
         * @private
         */
        EditorComponent.prototype.zone;
        /**
         * @type {?}
         * @private
         */
        EditorComponent.prototype.editorConfig;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var DiffEditorComponent = /** @class */ (function (_super) {
        __extends(DiffEditorComponent, _super);
        function DiffEditorComponent(editorConfig) {
            var _this = _super.call(this, editorConfig) || this;
            _this.editorConfig = editorConfig;
            return _this;
        }
        Object.defineProperty(DiffEditorComponent.prototype, "originalModel", {
            set: /**
             * @param {?} model
             * @return {?}
             */
            function (model) {
                this._originalModel = model;
                if (this._editor) {
                    this._editor.dispose();
                    this.initMonaco(this.options);
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DiffEditorComponent.prototype, "modifiedModel", {
            set: /**
             * @param {?} model
             * @return {?}
             */
            function (model) {
                this._modifiedModel = model;
                if (this._editor) {
                    this._editor.dispose();
                    this.initMonaco(this.options);
                }
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @protected
         * @param {?} options
         * @return {?}
         */
        DiffEditorComponent.prototype.initMonaco = /**
         * @protected
         * @param {?} options
         * @return {?}
         */
        function (options) {
            var _this = this;
            if (!this._originalModel || !this._modifiedModel) {
                throw new Error('originalModel or modifiedModel not found for ngx-monaco-diff-editor');
            }
            this._originalModel.language = this._originalModel.language || options.language;
            this._modifiedModel.language = this._modifiedModel.language || options.language;
            /** @type {?} */
            var originalModel = monaco.editor.createModel(this._originalModel.code, this._originalModel.language);
            /** @type {?} */
            var modifiedModel = monaco.editor.createModel(this._modifiedModel.code, this._modifiedModel.language);
            this._editorContainer.nativeElement.innerHTML = '';
            /** @type {?} */
            var theme = options.theme;
            this._editor = monaco.editor.createDiffEditor(this._editorContainer.nativeElement, options);
            options.theme = theme;
            this._editor.setModel({
                original: originalModel,
                modified: modifiedModel
            });
            // refresh layout on resize event.
            if (this._windowResizeSubscription) {
                this._windowResizeSubscription.unsubscribe();
            }
            this._windowResizeSubscription = rxjs.fromEvent(window, 'resize').subscribe((/**
             * @return {?}
             */
            function () { return _this._editor.layout(); }));
            this.onInit.emit(this._editor);
        };
        DiffEditorComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'ngx-monaco-diff-editor',
                        template: '<div class="editor-container" #editorContainer></div>',
                        styles: ["\n    :host {\n      display: block;\n      height: 200px;\n    }\n\n    .editor-container {\n      width: 100%;\n      height: 98%;\n    }\n  "]
                    }] }
        ];
        /** @nocollapse */
        DiffEditorComponent.ctorParameters = function () { return [
            { type: undefined, decorators: [{ type: core.Inject, args: [NGX_MONACO_EDITOR_CONFIG,] }] }
        ]; };
        DiffEditorComponent.propDecorators = {
            originalModel: [{ type: core.Input, args: ['originalModel',] }],
            modifiedModel: [{ type: core.Input, args: ['modifiedModel',] }]
        };
        return DiffEditorComponent;
    }(BaseEditor));
    if (false) {
        /** @type {?} */
        DiffEditorComponent.prototype._originalModel;
        /** @type {?} */
        DiffEditorComponent.prototype._modifiedModel;
        /**
         * @type {?}
         * @private
         */
        DiffEditorComponent.prototype.editorConfig;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var MonacoEditorModule = /** @class */ (function () {
        function MonacoEditorModule() {
        }
        /**
         * @param {?=} config
         * @return {?}
         */
        MonacoEditorModule.forRoot = /**
         * @param {?=} config
         * @return {?}
         */
        function (config) {
            if (config === void 0) { config = {}; }
            return {
                ngModule: MonacoEditorModule,
                providers: [
                    { provide: NGX_MONACO_EDITOR_CONFIG, useValue: config }
                ]
            };
        };
        MonacoEditorModule.decorators = [
            { type: core.NgModule, args: [{
                        imports: [
                            common.CommonModule
                        ],
                        declarations: [
                            EditorComponent,
                            DiffEditorComponent
                        ],
                        exports: [
                            EditorComponent,
                            DiffEditorComponent
                        ]
                    },] }
        ];
        return MonacoEditorModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function DiffEditorModel() { }
    if (false) {
        /** @type {?} */
        DiffEditorModel.prototype.code;
        /** @type {?} */
        DiffEditorModel.prototype.language;
    }
    /**
     * @record
     */
    function NgxEditorModel() { }
    if (false) {
        /** @type {?} */
        NgxEditorModel.prototype.value;
        /** @type {?|undefined} */
        NgxEditorModel.prototype.language;
        /** @type {?|undefined} */
        NgxEditorModel.prototype.uri;
    }

    exports.EditorComponent = EditorComponent;
    exports.MonacoEditorModule = MonacoEditorModule;
    exports.NGX_MONACO_EDITOR_CONFIG = NGX_MONACO_EDITOR_CONFIG;
    exports.ɵa = BaseEditor;
    exports.ɵb = DiffEditorComponent;

    Object.defineProperty(exports, '__esModule', { value: true });

}));
//# sourceMappingURL=ngx-monaco-editor.umd.js.map
