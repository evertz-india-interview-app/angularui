import { EventEmitter, ViewChild, Output, Input, InjectionToken, Component, forwardRef, NgZone, Inject, NgModule } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { CommonModule } from '@angular/common';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
let loadedMonaco = false;
/** @type {?} */
let loadPromise;
/**
 * @abstract
 */
class BaseEditor {
    /**
     * @param {?} config
     */
    constructor(config) {
        this.config = config;
        this.onInit = new EventEmitter();
    }
    /**
     * @param {?} options
     * @return {?}
     */
    set options(options) {
        this._options = Object.assign({}, this.config.defaultOptions, options);
        if (this._editor) {
            this._editor.dispose();
            this.initMonaco(options);
        }
    }
    /**
     * @return {?}
     */
    get options() {
        return this._options;
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        if (loadedMonaco) {
            // Wait until monaco editor is available
            loadPromise.then((/**
             * @return {?}
             */
            () => {
                this.initMonaco(this.options);
            }));
        }
        else {
            loadedMonaco = true;
            loadPromise = new Promise((/**
             * @param {?} resolve
             * @return {?}
             */
            (resolve) => {
                /** @type {?} */
                const baseUrl = this.config.baseUrl || './assets';
                if (typeof (((/** @type {?} */ (window))).monaco) === 'object') {
                    resolve();
                    return;
                }
                /** @type {?} */
                const onGotAmdLoader = (/**
                 * @return {?}
                 */
                () => {
                    // Load monaco
                    ((/** @type {?} */ (window))).require.config({ paths: { 'vs': `${baseUrl}/monaco/vs` } });
                    ((/** @type {?} */ (window))).require(['vs/editor/editor.main'], (/**
                     * @return {?}
                     */
                    () => {
                        if (typeof this.config.onMonacoLoad === 'function') {
                            this.config.onMonacoLoad();
                        }
                        this.initMonaco(this.options);
                        resolve();
                    }));
                });
                // Load AMD loader if necessary
                if (!((/** @type {?} */ (window))).require) {
                    /** @type {?} */
                    const loaderScript = document.createElement('script');
                    loaderScript.type = 'text/javascript';
                    loaderScript.src = `${baseUrl}/monaco/vs/loader.js`;
                    loaderScript.addEventListener('load', onGotAmdLoader);
                    document.body.appendChild(loaderScript);
                }
                else {
                    onGotAmdLoader();
                }
            }));
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this._windowResizeSubscription) {
            this._windowResizeSubscription.unsubscribe();
        }
        if (this._editor) {
            this._editor.dispose();
            this._editor = undefined;
        }
    }
}
BaseEditor.propDecorators = {
    _editorContainer: [{ type: ViewChild, args: ['editorContainer', { static: true },] }],
    onInit: [{ type: Output }],
    options: [{ type: Input, args: ['options',] }]
};
if (false) {
    /** @type {?} */
    BaseEditor.prototype._editorContainer;
    /** @type {?} */
    BaseEditor.prototype.onInit;
    /**
     * @type {?}
     * @protected
     */
    BaseEditor.prototype._editor;
    /**
     * @type {?}
     * @private
     */
    BaseEditor.prototype._options;
    /**
     * @type {?}
     * @protected
     */
    BaseEditor.prototype._windowResizeSubscription;
    /**
     * @type {?}
     * @private
     */
    BaseEditor.prototype.config;
    /**
     * @abstract
     * @protected
     * @param {?} options
     * @return {?}
     */
    BaseEditor.prototype.initMonaco = function (options) { };
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const NGX_MONACO_EDITOR_CONFIG = new InjectionToken('NGX_MONACO_EDITOR_CONFIG');
/**
 * @record
 */
function NgxMonacoEditorConfig() { }
if (false) {
    /** @type {?|undefined} */
    NgxMonacoEditorConfig.prototype.baseUrl;
    /** @type {?|undefined} */
    NgxMonacoEditorConfig.prototype.defaultOptions;
    /** @type {?|undefined} */
    NgxMonacoEditorConfig.prototype.onMonacoLoad;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class EditorComponent extends BaseEditor {
    /**
     * @param {?} zone
     * @param {?} editorConfig
     */
    constructor(zone, editorConfig) {
        super(editorConfig);
        this.zone = zone;
        this.editorConfig = editorConfig;
        this._value = '';
        this.propagateChange = (/**
         * @param {?} _
         * @return {?}
         */
        (_) => { });
        this.onTouched = (/**
         * @return {?}
         */
        () => { });
    }
    /**
     * @param {?} model
     * @return {?}
     */
    set model(model) {
        this.options.model = model;
        if (this._editor) {
            this._editor.dispose();
            this.initMonaco(this.options);
        }
    }
    /**
     * @param {?} value
     * @return {?}
     */
    writeValue(value) {
        this._value = value || '';
        // Fix for value change while dispose in process.
        setTimeout((/**
         * @return {?}
         */
        () => {
            if (this._editor && !this.options.model) {
                this._editor.setValue(this._value);
            }
        }));
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnChange(fn) {
        this.propagateChange = fn;
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    /**
     * @protected
     * @param {?} options
     * @return {?}
     */
    initMonaco(options) {
        /** @type {?} */
        const hasModel = !!options.model;
        if (hasModel) {
            /** @type {?} */
            const model = monaco.editor.getModel(options.model.uri || '');
            if (model) {
                options.model = model;
                options.model.setValue(this._value);
            }
            else {
                options.model = monaco.editor.createModel(options.model.value, options.model.language, options.model.uri);
            }
        }
        this._editor = monaco.editor.create(this._editorContainer.nativeElement, options);
        if (!hasModel) {
            this._editor.setValue(this._value);
        }
        this._editor.onDidChangeModelContent((/**
         * @param {?} e
         * @return {?}
         */
        (e) => {
            /** @type {?} */
            const value = this._editor.getValue();
            this.propagateChange(value);
            // value is not propagated to parent when executing outside zone.
            this.zone.run((/**
             * @return {?}
             */
            () => this._value = value));
        }));
        this._editor.onDidBlurEditorWidget((/**
         * @return {?}
         */
        () => {
            this.onTouched();
        }));
        // refresh layout on resize event.
        if (this._windowResizeSubscription) {
            this._windowResizeSubscription.unsubscribe();
        }
        this._windowResizeSubscription = fromEvent(window, 'resize').subscribe((/**
         * @return {?}
         */
        () => this._editor.layout()));
        this.onInit.emit(this._editor);
    }
}
EditorComponent.decorators = [
    { type: Component, args: [{
                selector: 'ngx-monaco-editor',
                template: '<div class="editor-container" #editorContainer></div>',
                providers: [{
                        provide: NG_VALUE_ACCESSOR,
                        useExisting: forwardRef((/**
                         * @return {?}
                         */
                        () => EditorComponent)),
                        multi: true
                    }],
                styles: [`
    :host {
      display: block;
      height: 200px;
    }

    .editor-container {
      width: 100%;
      height: 98%;
    }
  `]
            }] }
];
/** @nocollapse */
EditorComponent.ctorParameters = () => [
    { type: NgZone },
    { type: undefined, decorators: [{ type: Inject, args: [NGX_MONACO_EDITOR_CONFIG,] }] }
];
EditorComponent.propDecorators = {
    model: [{ type: Input, args: ['model',] }]
};
if (false) {
    /**
     * @type {?}
     * @private
     */
    EditorComponent.prototype._value;
    /** @type {?} */
    EditorComponent.prototype.propagateChange;
    /** @type {?} */
    EditorComponent.prototype.onTouched;
    /**
     * @type {?}
     * @private
     */
    EditorComponent.prototype.zone;
    /**
     * @type {?}
     * @private
     */
    EditorComponent.prototype.editorConfig;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class DiffEditorComponent extends BaseEditor {
    /**
     * @param {?} editorConfig
     */
    constructor(editorConfig) {
        super(editorConfig);
        this.editorConfig = editorConfig;
    }
    /**
     * @param {?} model
     * @return {?}
     */
    set originalModel(model) {
        this._originalModel = model;
        if (this._editor) {
            this._editor.dispose();
            this.initMonaco(this.options);
        }
    }
    /**
     * @param {?} model
     * @return {?}
     */
    set modifiedModel(model) {
        this._modifiedModel = model;
        if (this._editor) {
            this._editor.dispose();
            this.initMonaco(this.options);
        }
    }
    /**
     * @protected
     * @param {?} options
     * @return {?}
     */
    initMonaco(options) {
        if (!this._originalModel || !this._modifiedModel) {
            throw new Error('originalModel or modifiedModel not found for ngx-monaco-diff-editor');
        }
        this._originalModel.language = this._originalModel.language || options.language;
        this._modifiedModel.language = this._modifiedModel.language || options.language;
        /** @type {?} */
        let originalModel = monaco.editor.createModel(this._originalModel.code, this._originalModel.language);
        /** @type {?} */
        let modifiedModel = monaco.editor.createModel(this._modifiedModel.code, this._modifiedModel.language);
        this._editorContainer.nativeElement.innerHTML = '';
        /** @type {?} */
        const theme = options.theme;
        this._editor = monaco.editor.createDiffEditor(this._editorContainer.nativeElement, options);
        options.theme = theme;
        this._editor.setModel({
            original: originalModel,
            modified: modifiedModel
        });
        // refresh layout on resize event.
        if (this._windowResizeSubscription) {
            this._windowResizeSubscription.unsubscribe();
        }
        this._windowResizeSubscription = fromEvent(window, 'resize').subscribe((/**
         * @return {?}
         */
        () => this._editor.layout()));
        this.onInit.emit(this._editor);
    }
}
DiffEditorComponent.decorators = [
    { type: Component, args: [{
                selector: 'ngx-monaco-diff-editor',
                template: '<div class="editor-container" #editorContainer></div>',
                styles: [`
    :host {
      display: block;
      height: 200px;
    }

    .editor-container {
      width: 100%;
      height: 98%;
    }
  `]
            }] }
];
/** @nocollapse */
DiffEditorComponent.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [NGX_MONACO_EDITOR_CONFIG,] }] }
];
DiffEditorComponent.propDecorators = {
    originalModel: [{ type: Input, args: ['originalModel',] }],
    modifiedModel: [{ type: Input, args: ['modifiedModel',] }]
};
if (false) {
    /** @type {?} */
    DiffEditorComponent.prototype._originalModel;
    /** @type {?} */
    DiffEditorComponent.prototype._modifiedModel;
    /**
     * @type {?}
     * @private
     */
    DiffEditorComponent.prototype.editorConfig;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class MonacoEditorModule {
    /**
     * @param {?=} config
     * @return {?}
     */
    static forRoot(config = {}) {
        return {
            ngModule: MonacoEditorModule,
            providers: [
                { provide: NGX_MONACO_EDITOR_CONFIG, useValue: config }
            ]
        };
    }
}
MonacoEditorModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule
                ],
                declarations: [
                    EditorComponent,
                    DiffEditorComponent
                ],
                exports: [
                    EditorComponent,
                    DiffEditorComponent
                ]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function DiffEditorModel() { }
if (false) {
    /** @type {?} */
    DiffEditorModel.prototype.code;
    /** @type {?} */
    DiffEditorModel.prototype.language;
}
/**
 * @record
 */
function NgxEditorModel() { }
if (false) {
    /** @type {?} */
    NgxEditorModel.prototype.value;
    /** @type {?|undefined} */
    NgxEditorModel.prototype.language;
    /** @type {?|undefined} */
    NgxEditorModel.prototype.uri;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { EditorComponent, MonacoEditorModule, NGX_MONACO_EDITOR_CONFIG, BaseEditor as ɵa, DiffEditorComponent as ɵb };
//# sourceMappingURL=ngx-monaco-editor.js.map
