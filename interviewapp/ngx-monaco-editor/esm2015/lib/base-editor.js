/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
/** @type {?} */
let loadedMonaco = false;
/** @type {?} */
let loadPromise;
/**
 * @abstract
 */
export class BaseEditor {
    /**
     * @param {?} config
     */
    constructor(config) {
        this.config = config;
        this.onInit = new EventEmitter();
    }
    /**
     * @param {?} options
     * @return {?}
     */
    set options(options) {
        this._options = Object.assign({}, this.config.defaultOptions, options);
        if (this._editor) {
            this._editor.dispose();
            this.initMonaco(options);
        }
    }
    /**
     * @return {?}
     */
    get options() {
        return this._options;
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        if (loadedMonaco) {
            // Wait until monaco editor is available
            loadPromise.then((/**
             * @return {?}
             */
            () => {
                this.initMonaco(this.options);
            }));
        }
        else {
            loadedMonaco = true;
            loadPromise = new Promise((/**
             * @param {?} resolve
             * @return {?}
             */
            (resolve) => {
                /** @type {?} */
                const baseUrl = this.config.baseUrl || './assets';
                if (typeof (((/** @type {?} */ (window))).monaco) === 'object') {
                    resolve();
                    return;
                }
                /** @type {?} */
                const onGotAmdLoader = (/**
                 * @return {?}
                 */
                () => {
                    // Load monaco
                    ((/** @type {?} */ (window))).require.config({ paths: { 'vs': `${baseUrl}/monaco/vs` } });
                    ((/** @type {?} */ (window))).require(['vs/editor/editor.main'], (/**
                     * @return {?}
                     */
                    () => {
                        if (typeof this.config.onMonacoLoad === 'function') {
                            this.config.onMonacoLoad();
                        }
                        this.initMonaco(this.options);
                        resolve();
                    }));
                });
                // Load AMD loader if necessary
                if (!((/** @type {?} */ (window))).require) {
                    /** @type {?} */
                    const loaderScript = document.createElement('script');
                    loaderScript.type = 'text/javascript';
                    loaderScript.src = `${baseUrl}/monaco/vs/loader.js`;
                    loaderScript.addEventListener('load', onGotAmdLoader);
                    document.body.appendChild(loaderScript);
                }
                else {
                    onGotAmdLoader();
                }
            }));
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this._windowResizeSubscription) {
            this._windowResizeSubscription.unsubscribe();
        }
        if (this._editor) {
            this._editor.dispose();
            this._editor = undefined;
        }
    }
}
BaseEditor.propDecorators = {
    _editorContainer: [{ type: ViewChild, args: ['editorContainer', { static: true },] }],
    onInit: [{ type: Output }],
    options: [{ type: Input, args: ['options',] }]
};
if (false) {
    /** @type {?} */
    BaseEditor.prototype._editorContainer;
    /** @type {?} */
    BaseEditor.prototype.onInit;
    /**
     * @type {?}
     * @protected
     */
    BaseEditor.prototype._editor;
    /**
     * @type {?}
     * @private
     */
    BaseEditor.prototype._options;
    /**
     * @type {?}
     * @protected
     */
    BaseEditor.prototype._windowResizeSubscription;
    /**
     * @type {?}
     * @private
     */
    BaseEditor.prototype.config;
    /**
     * @abstract
     * @protected
     * @param {?} options
     * @return {?}
     */
    BaseEditor.prototype.initMonaco = function (options) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS1lZGl0b3IuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtbW9uYWNvLWVkaXRvci8iLCJzb3VyY2VzIjpbImxpYi9iYXNlLWVkaXRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFpQixVQUFVLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBYSxNQUFNLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDOztJQUl6RyxZQUFZLEdBQUcsS0FBSzs7SUFDcEIsV0FBMEI7Ozs7QUFHOUIsTUFBTSxPQUFnQixVQUFVOzs7O0lBb0I5QixZQUFvQixNQUE2QjtRQUE3QixXQUFNLEdBQU4sTUFBTSxDQUF1QjtRQWxCdkMsV0FBTSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7SUFrQlMsQ0FBQzs7Ozs7SUFickQsSUFDSSxPQUFPLENBQUMsT0FBWTtRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsY0FBYyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQ3ZFLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNoQixJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDMUI7SUFDSCxDQUFDOzs7O0lBRUQsSUFBSSxPQUFPO1FBQ1QsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDO0lBQ3ZCLENBQUM7Ozs7SUFJRCxlQUFlO1FBQ2IsSUFBSSxZQUFZLEVBQUU7WUFDaEIsd0NBQXdDO1lBQ3hDLFdBQVcsQ0FBQyxJQUFJOzs7WUFBQyxHQUFHLEVBQUU7Z0JBQ3BCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ2hDLENBQUMsRUFBQyxDQUFDO1NBQ0o7YUFBTTtZQUNMLFlBQVksR0FBRyxJQUFJLENBQUM7WUFDcEIsV0FBVyxHQUFHLElBQUksT0FBTzs7OztZQUFPLENBQUMsT0FBWSxFQUFFLEVBQUU7O3NCQUN6QyxPQUFPLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLElBQUksVUFBVTtnQkFDakQsSUFBSSxPQUFPLENBQUMsQ0FBQyxtQkFBSyxNQUFNLEVBQUEsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLFFBQVEsRUFBRTtvQkFDOUMsT0FBTyxFQUFFLENBQUM7b0JBQ1YsT0FBTztpQkFDUjs7c0JBQ0ssY0FBYzs7O2dCQUFRLEdBQUcsRUFBRTtvQkFDL0IsY0FBYztvQkFDZCxDQUFDLG1CQUFLLE1BQU0sRUFBQSxDQUFDLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEtBQUssRUFBRSxFQUFFLElBQUksRUFBRSxHQUFHLE9BQU8sWUFBWSxFQUFFLEVBQUUsQ0FBQyxDQUFDO29CQUMxRSxDQUFDLG1CQUFLLE1BQU0sRUFBQSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsdUJBQXVCLENBQUM7OztvQkFBRSxHQUFHLEVBQUU7d0JBQ3BELElBQUksT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksS0FBSyxVQUFVLEVBQUU7NEJBQ2xELElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxFQUFFLENBQUM7eUJBQzVCO3dCQUNELElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO3dCQUM5QixPQUFPLEVBQUUsQ0FBQztvQkFDWixDQUFDLEVBQUMsQ0FBQztnQkFDTCxDQUFDLENBQUE7Z0JBRUQsK0JBQStCO2dCQUMvQixJQUFJLENBQUMsQ0FBQyxtQkFBSyxNQUFNLEVBQUEsQ0FBQyxDQUFDLE9BQU8sRUFBRTs7MEJBQ3BCLFlBQVksR0FBc0IsUUFBUSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUM7b0JBQ3hFLFlBQVksQ0FBQyxJQUFJLEdBQUcsaUJBQWlCLENBQUM7b0JBQ3RDLFlBQVksQ0FBQyxHQUFHLEdBQUcsR0FBRyxPQUFPLHNCQUFzQixDQUFDO29CQUNwRCxZQUFZLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFLGNBQWMsQ0FBQyxDQUFDO29CQUN0RCxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsQ0FBQztpQkFDekM7cUJBQU07b0JBQ0wsY0FBYyxFQUFFLENBQUM7aUJBQ2xCO1lBQ0gsQ0FBQyxFQUFDLENBQUM7U0FDSjtJQUNILENBQUM7Ozs7SUFJRCxXQUFXO1FBQ1QsSUFBSSxJQUFJLENBQUMseUJBQXlCLEVBQUU7WUFDbEMsSUFBSSxDQUFDLHlCQUF5QixDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQzlDO1FBQ0QsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ2hCLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDdkIsSUFBSSxDQUFDLE9BQU8sR0FBRyxTQUFTLENBQUM7U0FDMUI7SUFDSCxDQUFDOzs7K0JBdkVBLFNBQVMsU0FBQyxpQkFBaUIsRUFBRSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUU7cUJBQzdDLE1BQU07c0JBS04sS0FBSyxTQUFDLFNBQVM7Ozs7SUFOaEIsc0NBQTZFOztJQUM3RSw0QkFBMkM7Ozs7O0lBQzNDLDZCQUF1Qjs7Ozs7SUFDdkIsOEJBQXNCOzs7OztJQUN0QiwrQ0FBa0Q7Ozs7O0lBZXRDLDRCQUFxQzs7Ozs7OztJQTBDakQseURBQWtEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQWZ0ZXJWaWV3SW5pdCwgRWxlbWVudFJlZiwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25EZXN0cm95LCBPdXRwdXQsIFZpZXdDaGlsZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBOZ3hNb25hY29FZGl0b3JDb25maWcgfSBmcm9tICcuL2NvbmZpZyc7XG5cbmxldCBsb2FkZWRNb25hY28gPSBmYWxzZTtcbmxldCBsb2FkUHJvbWlzZTogUHJvbWlzZTx2b2lkPjtcbmRlY2xhcmUgY29uc3QgcmVxdWlyZTogYW55O1xuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgQmFzZUVkaXRvciBpbXBsZW1lbnRzIEFmdGVyVmlld0luaXQsIE9uRGVzdHJveSB7XG4gIEBWaWV3Q2hpbGQoJ2VkaXRvckNvbnRhaW5lcicsIHsgc3RhdGljOiB0cnVlIH0pIF9lZGl0b3JDb250YWluZXI6IEVsZW1lbnRSZWY7XG4gIEBPdXRwdXQoKSBvbkluaXQgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcbiAgcHJvdGVjdGVkIF9lZGl0b3I6IGFueTtcbiAgcHJpdmF0ZSBfb3B0aW9uczogYW55O1xuICBwcm90ZWN0ZWQgX3dpbmRvd1Jlc2l6ZVN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xuXG4gIEBJbnB1dCgnb3B0aW9ucycpXG4gIHNldCBvcHRpb25zKG9wdGlvbnM6IGFueSkge1xuICAgIHRoaXMuX29wdGlvbnMgPSBPYmplY3QuYXNzaWduKHt9LCB0aGlzLmNvbmZpZy5kZWZhdWx0T3B0aW9ucywgb3B0aW9ucyk7XG4gICAgaWYgKHRoaXMuX2VkaXRvcikge1xuICAgICAgdGhpcy5fZWRpdG9yLmRpc3Bvc2UoKTtcbiAgICAgIHRoaXMuaW5pdE1vbmFjbyhvcHRpb25zKTtcbiAgICB9XG4gIH1cblxuICBnZXQgb3B0aW9ucygpOiBhbnkge1xuICAgIHJldHVybiB0aGlzLl9vcHRpb25zO1xuICB9XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBjb25maWc6IE5neE1vbmFjb0VkaXRvckNvbmZpZykge31cblxuICBuZ0FmdGVyVmlld0luaXQoKTogdm9pZCB7XG4gICAgaWYgKGxvYWRlZE1vbmFjbykge1xuICAgICAgLy8gV2FpdCB1bnRpbCBtb25hY28gZWRpdG9yIGlzIGF2YWlsYWJsZVxuICAgICAgbG9hZFByb21pc2UudGhlbigoKSA9PiB7XG4gICAgICAgIHRoaXMuaW5pdE1vbmFjbyh0aGlzLm9wdGlvbnMpO1xuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGxvYWRlZE1vbmFjbyA9IHRydWU7XG4gICAgICBsb2FkUHJvbWlzZSA9IG5ldyBQcm9taXNlPHZvaWQ+KChyZXNvbHZlOiBhbnkpID0+IHtcbiAgICAgICAgY29uc3QgYmFzZVVybCA9IHRoaXMuY29uZmlnLmJhc2VVcmwgfHwgJy4vYXNzZXRzJztcbiAgICAgICAgaWYgKHR5cGVvZiAoKDxhbnk+d2luZG93KS5tb25hY28pID09PSAnb2JqZWN0Jykge1xuICAgICAgICAgIHJlc29sdmUoKTtcbiAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgY29uc3Qgb25Hb3RBbWRMb2FkZXI6IGFueSA9ICgpID0+IHtcbiAgICAgICAgICAvLyBMb2FkIG1vbmFjb1xuICAgICAgICAgICg8YW55PndpbmRvdykucmVxdWlyZS5jb25maWcoeyBwYXRoczogeyAndnMnOiBgJHtiYXNlVXJsfS9tb25hY28vdnNgIH0gfSk7XG4gICAgICAgICAgKDxhbnk+d2luZG93KS5yZXF1aXJlKFsndnMvZWRpdG9yL2VkaXRvci5tYWluJ10sICgpID0+IHtcbiAgICAgICAgICAgIGlmICh0eXBlb2YgdGhpcy5jb25maWcub25Nb25hY29Mb2FkID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICAgIHRoaXMuY29uZmlnLm9uTW9uYWNvTG9hZCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5pbml0TW9uYWNvKHRoaXMub3B0aW9ucyk7XG4gICAgICAgICAgICByZXNvbHZlKCk7XG4gICAgICAgICAgfSk7XG4gICAgICAgIH07XG5cbiAgICAgICAgLy8gTG9hZCBBTUQgbG9hZGVyIGlmIG5lY2Vzc2FyeVxuICAgICAgICBpZiAoISg8YW55PndpbmRvdykucmVxdWlyZSkge1xuICAgICAgICAgIGNvbnN0IGxvYWRlclNjcmlwdDogSFRNTFNjcmlwdEVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzY3JpcHQnKTtcbiAgICAgICAgICBsb2FkZXJTY3JpcHQudHlwZSA9ICd0ZXh0L2phdmFzY3JpcHQnO1xuICAgICAgICAgIGxvYWRlclNjcmlwdC5zcmMgPSBgJHtiYXNlVXJsfS9tb25hY28vdnMvbG9hZGVyLmpzYDtcbiAgICAgICAgICBsb2FkZXJTY3JpcHQuYWRkRXZlbnRMaXN0ZW5lcignbG9hZCcsIG9uR290QW1kTG9hZGVyKTtcbiAgICAgICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKGxvYWRlclNjcmlwdCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgb25Hb3RBbWRMb2FkZXIoKTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfVxuICB9XG5cbiAgcHJvdGVjdGVkIGFic3RyYWN0IGluaXRNb25hY28ob3B0aW9uczogYW55KTogdm9pZDtcblxuICBuZ09uRGVzdHJveSgpIHtcbiAgICBpZiAodGhpcy5fd2luZG93UmVzaXplU3Vic2NyaXB0aW9uKSB7XG4gICAgICB0aGlzLl93aW5kb3dSZXNpemVTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcbiAgICB9XG4gICAgaWYgKHRoaXMuX2VkaXRvcikge1xuICAgICAgdGhpcy5fZWRpdG9yLmRpc3Bvc2UoKTtcbiAgICAgIHRoaXMuX2VkaXRvciA9IHVuZGVmaW5lZDtcbiAgICB9XG4gIH1cbn1cbiJdfQ==